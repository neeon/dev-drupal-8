## Featrures module configs & template

### How install?
1. `git clone https://neeon@bitbucket.org/neeon/dev-drupal-8.git`
2. Paste folder 'core' 
3. `composer install`, `composer update`
4. Go to the directory and install CMS Drupal 8
5. Install and set as default theme 'Dev Nix Film' (Admin Menu > Appearance)
6. Install this modules (Admin Menu > Extend): Block menu, Features, Features UI
7. Menu Configuration - Features - CustomFeaturesFilm - Change. Select all and import change.
8. Use the following block structure: https://goo.gl/4EzaZV
