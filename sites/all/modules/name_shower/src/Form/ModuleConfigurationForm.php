<?php

namespace Drupal\name_shower\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Устанавливает конфигурации для формы редактироа
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'name_shower_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'name_shower.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('name_shower.settings');
    $form['name_admin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Введите имя администратора сайта'),
      '#default_value' => $config->get('name_admin'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('name_shower.settings')
      ->set('name_admin', $values['name_admin'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}