<?php

namespace Drupal\name_shower\Plugin\Block;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Name Shower' Block.
 *
 * @Block(
 *   id = "name_shower",
 *   admin_label = @Translation("Show Name Block"),
 *   category = @Translation("Custom"),
 * )
 */
class ShowNameBlock extends BlockBase implements BlockPluginInterface {

  private function gethtmlBlock($font_size, $name_admin) {
    $html = '<p style="font-size: 15px;">' . $name_admin . '</p>';

    return $html;
  }

  /**
   * Выводит содержимое блока.
   *
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $config_module = \Drupal::config('name_shower.settings');

    $font_size = $config['font_size'];
    $name_admin = $config_module->get('name_admin');

    $html_block = $this->gethtmlBlock($font_size, $name_admin);

    return array(
      '#markup' => $html_block
    );
  }


  /**
   * Реализует форму конфигурации блока.
   *
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['font_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Введите размер шрифта для надписи'),
      '#description' => $this->t(''),
      '#default_value' => isset($config['font_size']) ? $config['font_size'] : '',
    ];

    return $form;
  }

  /**
   * Сохраняет конфигурации блока.
   *
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['font_size'] = $values['font_size'];
  }
}
