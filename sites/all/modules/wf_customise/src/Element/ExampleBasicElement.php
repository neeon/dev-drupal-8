<?php

namespace Drupal\wf_customise\Element;

use \Drupal\Core\Render\Element\Textfield;

/**
 * Implement basic form element for webform: 'example_basic_element'.
 *
 * Class ExampleBasicElement.
 *
 * @package Drupal\wf_customise\Element
 * @FormElement("example_basic_element")
 */
class ExampleBasicElement extends Textfield {
  
}