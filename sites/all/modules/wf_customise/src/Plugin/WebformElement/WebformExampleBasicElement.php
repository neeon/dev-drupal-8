<?php

namespace Drupal\wf_customise\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\TextField;
use Drupal\webform\WebformSubmissionInterface;


/**
 * Class WebformExampleBasicElement
 *
 * @WebformElement(
 *   id = "example_basic_element",
 *   label = @Translation("My Custom TextField"),
 *   description = @Translation("Provides a webform element example."),
 *   category = @Translation("Example elements"),
 * )
 *
 * @package Drupal\wf_customise\Plugin\WebformElement
 */
class WebformExampleBasicElement extends TextField {
}