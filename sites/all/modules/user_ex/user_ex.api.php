<?php
/**
 * @file
 *
 * Hooks of module 'User Extension'
 */

/**
 * Overrides messages for the user in case of successful authorization.

 * @param array $messages
 */
function hook_show_messages_after_success_auth_alter(array &$messages) {
  $messages = [
    'You are in the system.',
    'Hello'
  ];
}