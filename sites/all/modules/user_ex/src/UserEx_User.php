<?php
namespace Drupal\user_ex;

use \Drupal;

class User {
  /**
   * Checks whether the user is a guest.
   *
   * @return bool
   *    Are user guest?
   */
  public static function is_user_guest() {
    $roles = Drupal::currentUser()->getRoles();

    if (in_array('anonymous', $roles)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Generate the panel to display the status of the user.
   *
   * @return string
   */
  public static function generate_panel_status() {
    if (!UserEx::is_admin_panel()) {
      $class_panel = 'is_guest';

      if (User::is_user_guest()) {
        $value = 'Вы вошли как гость. <a href="/user/login">Авторизоваться</a>?';
      }
      else {
        $user_name = Drupal::currentUser()->getDisplayName();
        $value = 'Приветсвуем, <b>' . $user_name . '</b>! ';
        $value .= 'Перейти в <a href="/user">профиль</a>?';
        $class_panel = 'is_user';
      }

      return '<div class="user_ex__panel_status ' . $class_panel . '">
       <span class="active"></span>' . $value . '</div>';
    }
  }

  /**
   * Registers hook 'hook_show_messages_after_success_auth_alert'.
   * Hook displays a message on the page in case of successful authorization.
   */
  public static function hook_register__show_messages_after_success_auth() {
    $messages = ['Вы успешно авторизированы!'];

    Drupal::moduleHandler()
      ->alter('show_messages_after_success_auth', $messages);
    if (!empty($messages)) {
      foreach ($messages as $message) {
        Drupal::messenger()->addMessage($message);
      }
    }
  }
}
