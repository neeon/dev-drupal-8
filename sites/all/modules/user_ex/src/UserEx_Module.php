<?php
namespace Drupal\user_ex;

class UserEx {
  /**
   * Registers the specified libraries for the site.
   *
   * @param $variables
   */
  public static function register_libraries(&$variables) {
    $variables['#attached']['library'][] = 'user_ex/global-css';
  }

  /**
   * Checks whether the page is part of the admin panel.
   *
   * @return bool
   *    Page from the admin panel?
   */
  public static function is_admin_panel() {
    $admin_context = \Drupal::service('router.admin_context');
    if ($admin_context->isAdminRoute()) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}