jQuery(document).ready(function () {
    // Slider for horror films block
    jQuery('.films-horrors-slider').owlCarousel({
        nestedItemSelector: 'views-row',
        loop: true,
        margin: 10,
        nav: true,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000
    });

// Show menu
    jQuery('.header-menu ul.clearfix.menu  li.menu-item--expanded').on('mouseenter', function () {
        var submenu = jQuery(this).find('ul')[0];
        jQuery(submenu).fadeIn('fast');
    });

    jQuery(document).mouseup(function (e) {
        $container_parent = jQuery(".header-menu ul.clearfix.menu  li.menu-item--expanded");
        $container_children = jQuery(".header-menu ul.clearfix.menu  li.menu-item--expanded ul");

        if ($container_parent.has(e.target).length === 0 || $container_children.has(e.target).length === 0){
            $container_children.hide();
        }
    });

});